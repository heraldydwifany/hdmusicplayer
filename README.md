
# HD Music Player iOS App
[![N|Solid](https://gitlab.com/heraldydwifany/hdmusicplayer/badges/master/pipeline.svg?style=flat)](https://gitlab.com/heraldydwifany/hdmusicplayer/pipelines)

This app was developed using these specifications:
 - Swift Version: 5
 - Min. iOS Version: 13
 - XCode Version: 15

# Project Structure
the project has the following top level groups:
1. **Core**: Contains component which is wrapping framework level API.
2. **Configurations**: Contains required configuration files in .plist format
3. **Common**: Contains classes that are commonly used
4. **Features**: Contains all available feature for supporting usecases.

# Architecture
This project architecture is based on VIPER architecture pattern. but without Routing, since this app only has single screen.
These are our VIPER components responsibilities:
- View: Handled by *Controller* class
- Interactor: Handled by *Interactor* class
- Presenter: Handled by *Presenter* class
- Entity: Handled by *IRequest* for retrieving from remote server

# CI/CD
This repositories already implemented CI/CD with gitlab CI and Fastlane running in 2019 Macbook Pro Runner, and it has 2 stages:
- Test: will be running unit test on the project
- Deploy: deploying the app into firebase app distribution
