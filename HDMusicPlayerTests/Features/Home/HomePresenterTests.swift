//
//  HomePresenterTests.swift
//  HDMusicPlayerTests
//
//  Created by Heraldy Dwifany on 28/10/23.
//

import XCTest
@testable import HDMusicPlayer

final class HomeTests: XCTestCase {
    func testInit() throws{
        let _ = try makeSUT()
    }
    
    func testGetTracks() throws{
        let sut = try makeSUT()
        
        sut.viewModel.tracks = getTracks(2)
        
        XCTAssertNotNil(sut.getTracks())
        
        sut.viewModel.tracks = nil
        
        XCTAssertNil(sut.getTracks())
    }
    
    func testGetTrackCount() throws{
        let sut = try makeSUT()
        
        XCTAssertEqual(sut.getTracksCount(), 0)
        
        sut.viewModel.tracks = getTracks(3)
        
        XCTAssertEqual(sut.getTracksCount(), 3)
    }
    
    func testGetCurrentTrack() throws{
        let sut = try makeSUT()
        
        sut.viewModel.currentTrack = getTrack(id: 2)
        
        XCTAssertNotNil(sut.getCurrentTrack())
        
        sut.viewModel.currentTrack = nil
        
        XCTAssertNil(sut.getCurrentTrack())
    }
    
    func testGetCurrentIndex() throws{
        let sut = try makeSUT()
        
        sut.viewModel.currentIndex = 1
        
        let index = try XCTUnwrap(sut.viewModel.currentIndex)
        
        XCTAssertEqual(index, 1)
        XCTAssertNotNil(sut.getCurrentIndex())
        
        sut.viewModel.currentIndex = nil
        
        XCTAssertNil(sut.getCurrentIndex())
    }
    
    func testGetSearchText() throws{
        let sut = try makeSUT()
        
        sut.viewModel.searchText = "Artist 2"
        
        XCTAssertEqual(sut.getSearchText(), "Artist 2")
        
        sut.viewModel.searchText = nil
        
        XCTAssertTrue(sut.getSearchText().isEmpty)
    }
    
    func testGetEmptyState() throws{
        let sut = try makeSUT()
        
        sut.viewModel.searchText = nil
        
        XCTAssertEqual(sut.getEmptyState(), .empty)
        
        sut.viewModel.searchText = "Artist 2"
        sut.viewModel.tracks = nil
        
        XCTAssertEqual(sut.getEmptyState(), .notFound)
        
        sut.viewModel.searchText = "Artist 3"
        sut.viewModel.tracks = getTracks(2)
        
        XCTAssertEqual(sut.getEmptyState(), .none)
    }
    
    func testSetSearchText() throws{
        let sut = try makeSUT()
        
        sut.setSearchText("Artist 10")
        
        XCTAssertEqual(sut.viewModel.searchText, "Artist 10")
        
        sut.setSearchText("Artist 121")
        
        XCTAssertEqual(sut.viewModel.searchText, "Artist 121")
    }
    
    func testDidSelectSubmit() throws{
        let sut = try makeSUT()
        
        sut.viewModel.currentIndex = 10
        
        XCTAssertEqual(sut.viewModel.currentIndex, 10)
        
        sut.didSelectSubmit()
        
        XCTAssertNil(sut.viewModel.currentIndex)
    }
    
    func testDidSelectCell() throws{
        let sut = try makeSUT()
        
        sut.viewModel.tracks = getTracks(5)
        
        sut.didSelectCell(index: 1)
        
        XCTAssertNotNil(sut.viewModel.currentTrack)
        XCTAssertNotNil(sut.viewModel.currentIndex)
        
        XCTAssertEqual(sut.viewModel.currentTrack?.name, "Track 2")
        XCTAssertEqual(sut.viewModel.currentIndex, 1)
        
        sut.viewModel.tracks = nil
        
        sut.didSelectCell(index: 3)
        
        XCTAssertNotNil(sut.viewModel.currentTrack)
        XCTAssertNotNil(sut.viewModel.currentIndex)
        
        XCTAssertEqual(sut.viewModel.currentTrack?.name, "Track 2")
        XCTAssertEqual(sut.viewModel.currentIndex, 1)
    }
    
    func testDidSelectNextTrack() throws{
        let sut = try makeSUT()
        
        sut.viewModel.tracks = getTracks(20)
        sut.viewModel.currentIndex = 0
        
        sut.didSelectNextTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 1)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 2")
        
        sut.viewModel.currentIndex = 19
        
        sut.didSelectNextTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 0)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 1")
        
        sut.viewModel.currentIndex = nil
        
        sut.didSelectNextTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 0)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 1")
        
        sut.viewModel.tracks = nil
        
        sut.didSelectNextTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 0)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 1")
    }
    
    func testDidSelectPreviousTrack() throws{
        let sut = try makeSUT()
        
        sut.viewModel.tracks = getTracks(20)
        sut.viewModel.currentIndex = 0
        
        sut.didSelectPreviousTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 19)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 20")
        
        sut.viewModel.currentIndex = 19
        
        sut.didSelectPreviousTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 18)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 19")
        
        sut.viewModel.currentIndex = nil
        
        sut.didSelectPreviousTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 19)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 20")
        
        sut.viewModel.tracks = nil
        
        sut.didSelectPreviousTrack()
        
        XCTAssertEqual(sut.getCurrentIndex(), 19)
        XCTAssertEqual(sut.getCurrentTrack()?.name, "Track 20")
    }
    
    func testDidTrackEnded() throws{
        let sut = try makeSUT()
        
        sut.viewModel.currentIndex = 5
        sut.viewModel.currentTrack = getTrack(id: 6)
        
        XCTAssertNotNil(sut.viewModel.currentIndex)
        XCTAssertNotNil(sut.viewModel.currentTrack)
        
        sut.didTrackEnded()
        
        XCTAssertNil(sut.viewModel.currentIndex)
        XCTAssertNil(sut.viewModel.currentTrack)
    }

    private func makeSUT() throws ->HomePresenter{
        let interactor = HomeInteractor(request: HTTPRequestMock())
        let viewModel = HomeViewModel()
        let presenter = HomePresenter(interactor: interactor, viewModel: viewModel)
        let vc = HomeViewController(presenter: presenter, player: AudioPlayerManagerMock())
        presenter.controller = vc
        
        return presenter
    }
    
    private func getTracks(_ n: Int)->[TrackModel]{
        var tracks: [TrackModel] = []
        for i in 1...n{
            tracks.append(getTrack(id: i))
        }
        return tracks
    }
    
    private func getTrack(id: Int)->TrackModel{
        return TrackModel(id: id, name: "Track \(id)",
                          artist: "Artist \(id)",
                          album: "Album \(id)",
                          albumCoverUrl: "Cover Url \(id)",
                          previewTrackUrl: "Preview Track Url \(id)")
    }
}


