//
//  AudioPlayerManagerMock.swift
//  HDMusicPlayerTests
//
//  Created by Heraldy Dwifany on 28/10/23.
//

import UIKit
@testable import HDMusicPlayer

class AudioPlayerManagerMock: IAudioPlayer{
   
    
    func setDelegate(_ vc: UIViewController) {
            
    }
    
    func loadTrack(url: String?) {
        
    }
    
    func didSelectPlayOrPauseTrack() {
        
    }
    
    func didSeekPlayer(_ value: Double) {
        
    }
    
    func getAudioPlayerState() -> HDMusicPlayer.AudioPlayerState {
        return .playing
    }
    
}
