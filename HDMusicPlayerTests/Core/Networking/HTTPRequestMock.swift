//
//  HTTPRequestMock.swift
//  HDMusicPlayerTests
//
//  Created by Heraldy Dwifany on 28/10/23.
//

import Foundation
@testable import HDMusicPlayer

class HTTPRequestMock: IHTTPRequest{
    func call(url: String, completion: @escaping ((Data?, Error?) -> ())) {
        completion(nil, nil)
    }
}
