//
//  TrackModel.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import Foundation

struct TrackResultModel: Codable{
    let results: [TrackModel]?
    
    enum CodingKeys: String, CodingKey{
        case results = "results"
    }
}

struct TrackModel: Codable{
    let id: Int?
    let name: String?
    let artist: String?
    let album: String?
    let albumCoverUrl: String?
    let previewTrackUrl: String?
    
    enum CodingKeys: String, CodingKey{
        case id = "trackId"
        case name = "trackName"
        case artist = "artistName"
        case album = "collectionName"
        case albumCoverUrl = "artworkUrl100"
        case previewTrackUrl = "previewUrl"
    }
}
