//
//  TrackTableViewCell.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import UIKit

class TrackTableViewCell: UITableViewCell {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    @IBOutlet weak var indicatorImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(image: String?, name: String?, artist: String?, album: String?, isPlaying: Bool?){
        coverImageView.load(url: image)
        songNameLabel.text = name ?? "-"
        artistLabel.text = artist ?? "-"
        albumLabel.text = album ?? "-"
        indicatorImageView.isHidden = !(isPlaying ?? false)
    }
}
