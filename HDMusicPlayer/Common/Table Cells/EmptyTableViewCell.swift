//
//  EmptyTableViewCell.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 28/10/23.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }

    func bind(image: UIImage?, description: String){
        iconImageView.image = image
        descriptionLabel.text = description
    }
    
}
