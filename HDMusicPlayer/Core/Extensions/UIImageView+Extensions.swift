//
//  UIImageView+Extensions.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import UIKit
import Kingfisher

extension UIImageView{
    func load(url: String?){
        if let url = url, let _url = URL(string: url){
            self.kf.indicatorType = .activity
            self.kf.setImage(with: _url)
        }
    }
}
