//
//  UITableView+Extensions.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import UIKit

extension UITableView{
    func registerNib(_ cells: [UITableViewCell.Type]){
        for cell in cells{
            self.register(UINib(nibName: String(describing: cell), bundle: nil), forCellReuseIdentifier: String(describing: cell))
        }
    }
}
