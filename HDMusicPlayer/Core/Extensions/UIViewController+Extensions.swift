//
//  UIViewController+Extensions.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 28/10/23.
//

import UIKit

extension UIViewController{
    func showErrorMessage(_ message: String?){
        popUpMessage(title: "Error", message: message)
    }
    
    private func popUpMessage(title: String?, message: String?){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK!", style: .default)
            alert.addAction(alertAction)
            self.present(alert, animated: true)
        }
    }
}
