//
//  HTTPRequest.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import UIKit

protocol IHTTPRequest{
    func call(url: String, completion: @escaping ((Data?, Error?)->()))
}

class HTTPRequest: IHTTPRequest{
    static let shared = HTTPRequest()
    
    func call(url: String, completion: @escaping ((Data?, Error?)->())){
        if let url = URL(string: url){
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let task = URLSession.shared.dataTask(with: request) { data, urlResponse, error in
                if let error = error{
                    completion(nil, error)
                }
                completion(data, nil)
            }
            task.resume()
        } else {
            completion(nil, nil)
        }
    }
}
