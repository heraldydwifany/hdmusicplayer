//
//  AudioPlayerManager.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import AVKit

protocol IAudioPlayer{
    func setDelegate(_ vc: UIViewController)
    func loadTrack(url: String?)
    func didSelectPlayOrPauseTrack()
    func getAudioPlayerState()->AudioPlayerState
    func didSeekPlayer(_ value: Double)
}

protocol AudioPlayerManagerDelegate: AnyObject{
    func reloadState()
    func updateCurrentime(_ value: Double)
    func trackDidEnd()
    func didError(_ message: String)
}

class AudioPlayerManager: IAudioPlayer{
    static let shared = AudioPlayerManager()
    
    weak var delegate: AudioPlayerManagerDelegate?
    var audioPlayer: AVPlayer?
    
    func setDelegate(_ vc: UIViewController) {
        self.delegate = vc as? AudioPlayerManagerDelegate
    }
    
    func loadTrack(url: String?){
        NotificationCenter.default.removeObserver(self)
        
        guard let url = url, let _url = URL(string: url) else { return }
        let playerItem = AVPlayerItem(url: _url)
        
        audioPlayer = AVPlayer(playerItem: playerItem)
        audioPlayer?.play()
        audioPlayer?.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 1), queue: .main, using: { [weak self] time in
            guard let duration = self?.audioPlayer?.currentItem?.duration.seconds else {return}
            let currenttime = time.seconds / duration
            self?.delegate?.updateCurrentime(currenttime)
        })
        
        if let error = audioPlayer?.error{
            delegate?.didError(error.localizedDescription)
            return
        }
        
        delegate?.reloadState()
        
        NotificationCenter.default.addObserver(self, selector: #selector(trackFinished), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    func didSelectPlayOrPauseTrack(){
        if getAudioPlayerState() == .playing{
            audioPlayer?.pause()
        } else {
            audioPlayer?.play()
        }
        delegate?.reloadState()
    }
    
    func getAudioPlayerState()->AudioPlayerState{
        return (audioPlayer?.rate != 0 && audioPlayer?.error == nil) ? .playing : .paused
    }
    
    func didSeekPlayer(_ value: Double) {
        if let second = audioPlayer?.currentItem?.duration.seconds{
            let _second = second * value
            audioPlayer?.seek(to: CMTimeMakeWithSeconds(Float64(_second), preferredTimescale: 1000))
        }
    }
    
    @objc private func trackFinished(){
        delegate?.trackDidEnd()
        delegate?.reloadState()
        NotificationCenter.default.removeObserver(self)
    }
}

enum AudioPlayerState{
    case playing
    case paused
    
    var icon: UIImage{
        switch self{
        case .paused:
            return UIImage(systemName: "play.fill")!
        case .playing:
            return UIImage(systemName: "pause.fill")!
        }
    }
}
