//
//  HomeViewController.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import UIKit

class HomeViewController: UIViewController{
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerPlayButton: UIButton!
    @IBOutlet weak var playerSlider: UISlider!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    let presenter: IHomePresenter
    let player: IAudioPlayer
    var debounceTimer: Timer? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setupTableView()
        setupPlayer()
        setupTextField()
    }
    
    init(presenter: IHomePresenter, player: IAudioPlayer){
        self.presenter = presenter
        self.player = player
        
        super.init(nibName: "HomeViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupNavigationBar(){
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func setupTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerNib([TrackTableViewCell.self,
                               EmptyTableViewCell.self ])
    }
    
    private func setupPlayer(){
        player.setDelegate(self)
    }
    
    private func setupIcon(){
        DispatchQueue.main.async {
            self.playerPlayButton.setImage(self.player.getAudioPlayerState().icon, for: .normal)
        }
    }

    private func setupTextField(){
        searchTextField.delegate = self
    }
    
    @objc private func submitText(){
        print("HIT:\(Date())")
        presenter.didSelectSubmit()
        debounceTimer = nil
    }
    
    private func setTimer(){
        debounceTimer?.invalidate()
        debounceTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(submitText), userInfo: nil, repeats: false)
    }
}

extension HomeViewController{
    @IBAction func didSelectDone(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func previousButtonAction(_ sender: Any) {
        playerSlider.value = 0.0
        presenter.didSelectPreviousTrack()
    }
    
    @IBAction func playButtonAction(_ sender: Any) {
        player.didSelectPlayOrPauseTrack()
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        playerSlider.value = 0.0
        presenter.didSelectNextTrack()
    }
    
    @IBAction func sliderChangedAction(_ sender: UISlider) {
        player.didSeekPlayer(Double(sender.value))
    }
}

extension HomeViewController: AudioPlayerManagerDelegate{
    func reloadState() {
        setupIcon()
    }
    
    func updateCurrentime(_ value: Double) {
        playerSlider.value = Float(value)
    }
    
    func trackDidEnd() {
        presenter.didTrackEnded()
    }
    
    func didError(_ message: String) {
        showErrorMessage(message)
    }
}

extension HomeViewController: IHomeViewController{
    func reloadData(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showLoading(_ show: Bool){
        DispatchQueue.main.async {
            self.loadingIndicator.isHidden = !show
        }
    }
    
    func reloadUI() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.1, delay: 0, options: .transitionCrossDissolve, animations: {
                self.playerView.isHidden = self.presenter.getCurrentTrack() == nil
                self.playerImageView.load(url: self.presenter.getCurrentTrack()?.albumCoverUrl)
            }, completion: nil)
        }
    }
    
    func play(url: String?) {
        player.loadTrack(url: url)
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getEmptyState() == .none ? presenter.getTracksCount() : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if presenter.getEmptyState() == .none{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "TrackTableViewCell", for: indexPath) as? TrackTableViewCell,
               let items = presenter.getTracks(){
                cell.bind(image: items[indexPath.row].albumCoverUrl,
                          name: items[indexPath.row].name,
                          artist: items[indexPath.row].artist,
                          album: items[indexPath.row].album,
                          isPlaying: items[indexPath.row].id == presenter.getCurrentTrack()?.id)
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as? EmptyTableViewCell{
                cell.bind(image: UIImage(systemName: presenter.getEmptyState() == .empty ? "magnifyingglass" : "waveform.badge.magnifyingglass"),
                          description: presenter.getEmptyState() == .empty ? "Start Searching" : "Search Not Found")
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if presenter.getEmptyState() == .none{
            presenter.didSelectCell(index: indexPath.row)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if presenter.getEmptyState() == .none{
            return UITableView.automaticDimension
        }
        return tableView.frame.height
    }
}

extension HomeViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text{
            presenter.setSearchText(text)
        }
        setTimer()
        return true
    }
}
