//
//  HomeViewModel.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import Foundation

class HomeViewModel{
    var searchText: String?
    var currentIndex: Int?
    var currentTrack: TrackModel?
    var tracks: [TrackModel]?
    var emptyState: HomeEmptyState{
        if searchText == nil{
            return .empty
        } else if searchText != nil && (tracks ?? []).isEmpty{
            return .notFound
        }
        return .none
    }
}

enum HomeEmptyState{
    case none
    case empty
    case notFound
}
