//
//  HomeModule.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import UIKit

class HomeModule{
    func create()->UIViewController{
        let interactor = HomeInteractor(request: HTTPRequest.shared)
        let viewModel = HomeViewModel()
        let presenter = HomePresenter(interactor: interactor, viewModel: viewModel)
        let vc = HomeViewController(presenter: presenter, player: AudioPlayerManager.shared)
        presenter.controller = vc
        
        return vc
    }
}

