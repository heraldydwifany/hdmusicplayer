//
//  HomeInteractor.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import Foundation

class HomeInteractor: IHomeInteractor{
    let request: IHTTPRequest
    
    init(request: IHTTPRequest) {
        self.request = request
    }
    
    func fetchTracks(text: String, completion: @escaping (([TrackModel]?, Error?) -> ())) {
        guard let textParam = text.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return}
        //since itunes apple api doesn't support pagination, limit to 20 data only
        let url = "https://itunes.apple.com/search?term=\(textParam)&media=music&limit=20"
        request.call(url: url) { data, error in
            if let data = data,
                let trackResults = try? JSONDecoder().decode(TrackResultModel.self, from: data){
                completion(trackResults.results, nil)
            } else {
                completion(nil, error)
            }
        }
    }
}
