//
//  HomePresenter.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import Foundation

class HomePresenter: IHomePresenter{
    let interactor: IHomeInteractor
    let viewModel: HomeViewModel
    weak var controller: HomeViewController?
    
    init(interactor: IHomeInteractor, viewModel: HomeViewModel) {
        self.interactor = interactor
        self.viewModel = viewModel
    }
    
    private func fetchData(){
        if let text = viewModel.searchText{
            controller?.showLoading(true)
            interactor.fetchTracks(text: text) { [weak self] tracks, error in
                guard let self = self else { return }
                if let error = error{
                    self.controller?.showErrorMessage(error.localizedDescription)
                } else {
                    self.viewModel.tracks = tracks
                    self.controller?.reloadData()
                }
                self.controller?.showLoading(self.viewModel.tracks == nil)
            }
        }
    }
    
    func getTracks()->[TrackModel]?{
        return viewModel.tracks
    }
    
    func getTracksCount()->Int{
        return viewModel.tracks?.count ?? .zero
    }
    
    func getCurrentTrack()->TrackModel?{
        return viewModel.currentTrack
    }
    
    func getCurrentIndex()->Int?{
        return viewModel.currentIndex
    }
    
    func getSearchText()->String{
        guard let searchText = viewModel.searchText else { return ""}
        return searchText
    }
    
    func getEmptyState()->HomeEmptyState{
        return viewModel.emptyState
    }
    
    func setSearchText(_ text: String){
        viewModel.searchText = text
    }
    
    func didSelectSubmit(){
        viewModel.currentIndex = nil
        fetchData()
    }
    
    func didSelectCell(index: Int) {
        guard let track = viewModel.tracks else { return }
        viewModel.currentTrack = track[index]
        viewModel.currentIndex = index
        controller?.reloadUI()
        controller?.reloadData()
        playTrack()
    }
    
    func didSelectNextTrack(){
        guard let track = viewModel.tracks else { return }
        if let index = viewModel.currentIndex{
            if (index + 1) >= getTracksCount(){
                viewModel.currentIndex = 0
                viewModel.currentTrack = track[0]
            } else {
                viewModel.currentIndex = index + 1
                viewModel.currentTrack = track[index + 1]
            }
        } else {
            viewModel.currentIndex = 0
            viewModel.currentTrack = track[0]
        }
        controller?.reloadUI()
        controller?.reloadData()
        playTrack()
    }
    
    func didSelectPreviousTrack(){
        guard let track = viewModel.tracks else { return }
        if let index = viewModel.currentIndex{
            if (index - 1) <= 0{
                viewModel.currentIndex = (getTracksCount() - 1)
                viewModel.currentTrack = track[getTracksCount() - 1]
            } else {
                viewModel.currentIndex = index - 1
                viewModel.currentTrack = track[index - 1]
            }
        } else {
            viewModel.currentIndex = (getTracksCount() - 1)
            viewModel.currentTrack = track[getTracksCount() - 1]
        }
        controller?.reloadUI()
        controller?.reloadData()
        playTrack()
    }
    
    func didTrackEnded(){
        viewModel.currentIndex = nil
        viewModel.currentTrack = nil
        controller?.reloadUI()
        controller?.reloadData()
    }
    
    private func playTrack(){
        controller?.play(url: viewModel.currentTrack?.previewTrackUrl)
    }
}
