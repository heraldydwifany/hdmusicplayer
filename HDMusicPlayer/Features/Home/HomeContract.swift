//
//  HomeContract.swift
//  HDMusicPlayer
//
//  Created by Heraldy Dwifany on 27/10/23.
//

import Foundation

protocol IHomeInteractor{
    func fetchTracks(text: String, completion: @escaping (([TrackModel]?, Error?)->()))
}

protocol IHomeViewController: AnyObject{
    func showErrorMessage(_ message: String?)
    func reloadData()
    func reloadUI()
    func play(url: String?)
}

protocol IHomePresenter{
    func getTracks()->[TrackModel]?
    func getTracksCount()->Int
    func getCurrentTrack()->TrackModel?
    func getSearchText()->String
    func setSearchText(_ text: String)
    func getEmptyState()->HomeEmptyState
    func didSelectSubmit()
    func didSelectCell(index: Int)
    func didSelectNextTrack()
    func didSelectPreviousTrack()
    func didTrackEnded()
}
